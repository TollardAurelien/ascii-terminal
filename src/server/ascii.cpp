#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <thread>
#include "opencv2/opencv.hpp"
#include <sys/socket.h> // For socket functions
#include <netinet/in.h> // For sockaddr_in
#include <arpa/inet.h>

#if defined(_WIN32)
#include <Windows.h>
#else
#include <sys/ioctl.h>
#endif

using namespace std;
using namespace cv;

typedef struct winSize
{
    unsigned int col;
    unsigned int row;
} winSize;

void getWindowSize(winSize *win)
{
    struct winsize w;
#if defined(_WIN32)
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    w->col = (int)(csbi.srWindow.Right - csbi.srWindow.Left + 1);
    w->row = (int)(csbi.srWindow.Bottom - csbi.srWindow.Top + 1);
#else
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    win->col = w.ws_col;
    win->row = w.ws_row;
#endif
}

void waitThread(int time, bool *finish)
{
    *finish = false;
    usleep(time);
    *finish = true;
}

int main(int argc, char const *argv[])
{
    winSize windowSize;
    getWindowSize(&windowSize);
    printf("lines %d\n", windowSize.row);
    printf("columns %d\n", windowSize.col);

    // Create a socket (IPv4, TCP)
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    int opt = 1;
    if (sockfd == 0)
    {
        std::cout << "Failed to create socket. errno: " << errno << std::endl;
        exit(EXIT_FAILURE);
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,
                   &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // Listen to port 9999 on any address
    sockaddr_in sockaddr;
    sockaddr.sin_family = AF_INET;
    sockaddr.sin_addr.s_addr = inet_addr(argv[1]);
    sockaddr.sin_port = htons(8080); // htons is necessary to convert a number to
                                     // network byte order
    if (::bind(sockfd, (struct sockaddr *)&sockaddr, sizeof(sockaddr)) < 0)
    {
        std::cout << "Failed to bind to port 6969. errno: " << errno << std::endl;
        exit(EXIT_FAILURE);
    }

    // Start listening. Hold at most 10 connections in the queue
    if (::listen(sockfd, 1) < 0)
    {
        std::cout << "Failed to listen on socket. errno: " << errno << std::endl;
        exit(EXIT_FAILURE);
    }

    // Grab a connection from the queue
    auto addrlen = sizeof(sockaddr);
    int connection = accept(sockfd, (struct sockaddr *)&sockaddr, (socklen_t *)&addrlen);
    if (connection < 0)
    {
        std::cout << "Failed to grab connection. errno: " << errno << std::endl;
        exit(EXIT_FAILURE);
    }

    VideoCapture camera(0);
    if (!camera.isOpened())
    {
        std::cerr << "ERROR: Could not open camera" << std::endl;
        return 1;
    }
    system("clear");

    Mat frame;

    Mat terminal = Mat(windowSize.row, windowSize.col, CV_8UC1);

    String charList = ".:-=+*#%@";
    char printed = '1';

    while (1)
    {
        camera >> frame;
        cvtColor(frame, frame, COLOR_RGB2GRAY);
        flip(frame, frame, ROTATE_180);

        int stepX = frame.size[0] / windowSize.row, stepY = frame.size[1] / windowSize.col;
        int square = stepX * stepY;
        int compteurX = 0, compteurY;
        stringstream buffer;
        static int compteur = 0;
        for (int i = 0; compteurX < windowSize.row; i += stepX, compteurX++)
        {
            for (int j = 0, compteurY = 0; compteurY < windowSize.col; j += stepY, compteurY++)
            {
                double mean = 0;
                for (int k = 0; k < stepX; k++)
                {
                    for (int l = 0; l < stepY; l++)
                    {
                        mean += frame.at<uint8_t>(i + k, j + l);
                    }
                }
                mean /= square;
                for (int m = charList.size(); m > 0; m--)
                    if (mean < 255 / m)
                    {
                        buffer << charList.at(m - 1);
                        break;
                    }
            }
        }

        // Read from the connection
        char readBuffer[windowSize.row * windowSize.col];
        ::recv(connection, readBuffer, windowSize.row * windowSize.col, MSG_WAITALL);
        // Send a message to the connection
        send(connection, buffer.str().c_str(), windowSize.row * windowSize.col, 0);

        string t(readBuffer);
        if (t.length() > (windowSize.row * windowSize.col))
        {
            t.resize(windowSize.row * windowSize.col);
            std::cout << t << endl;
        }

        if ((char)27 == (char)waitKey(10))
            break;

        char ready[1];
        do
        {
            ::read(connection, ready, 1);
        } while (ready[0] != printed);
        send(connection, ready, 1, 0);
    }
    close(connection);
    close(sockfd);
    return 0;
}
